## **Inntall ArgoCd**

```
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
kubectl port-forward svc/argocd-server -n argocd 8080:443
```

## Instalar o Kustomize

```
curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" | bash
sudo mv kustomize /usr/local/bin
```
## Para configurar e usar o Kustomize, siga estas etapas básicas:

Crie uma estrutura de diretórios para organizar seus arquivos de configuração do Kubernetes. Por exemplo:

```
mkdir -p meu-projeto/base
mkdir -p meu-projeto/overlays

```
##  Para configurar e usar o Kustomize, siga estas etapas básicas:

Crie uma estrutura de diretórios para organizar seus arquivos de configuração do Kubernetes. Por exemplo:

```
mkdir -p meu-projeto/base
mkdir -p meu-projeto/overlays
```

#### Dentro do diretório base, coloque os arquivos de configuração YAML que descrevem os recursos do Kubernetes para a configuração base do seu aplicativo.

#### Dentro do diretório overlays, crie subdiretórios para cada ambiente ou configuração específica. Por exemplo, você pode ter meu-projeto/overlays/producao, meu-projeto/overlays/desenvolvimento, etc.

#### Dentro de cada subdiretório de overlay, crie arquivos YAML que descrevam as personalizações específicas para aquele ambiente. Esses arquivos YAML conterão as modificações em relação à configuração base.

#### Use o comando kustomize build para gerar os recursos do Kubernetes combinando a configuração base e as personalizações dos overlays. Por exemplo:

```
kustomize build meu-projeto/overlays/producao

```

#### Isso irá gerar o YAML combinado resultante das configurações base e overlay para o ambiente de produção.

#### Você pode redirecionar a saída do comando kustomize build para aplicar os recursos no cluster Kubernetes. Por exemplo:

```
kustomize build meu-projeto/overlays/producao | kubectl apply -f -

```

#### Isso aplica os recursos do Kubernetes gerados diretamente no cluster.

Essas são apenas etapas básicas para começar a usar o Kustomize. O Kustomize possui recursos avançados, como patches, substituições e muito mais, para personalizar ainda mais suas configurações. Você pode consultar a documentação oficial do Kustomize para obter mais detalhes sobre como utilizar todos os recursos oferecidos pela ferramenta:
**https://kubectl.docs.kubernetes.io/references/kustomize**

# Blue-Green no Argo CD:

### Crie um diretório para o seu projeto:

 ```
mkdir meu-projeto
cd meu-projeto

```

## Crie os arquivos YAML para descrever a configuração do seu aplicativo. Por exemplo, você pode ter os seguintes arquivos:

**base/deployment.yaml:** Descrição do Deployment da versão base do seu aplicativo.
**base/service.yaml:** Descrição do Service da versão base do seu aplicativo.
**blue/deployment.yaml:** Descrição do Deployment da versão azul do seu aplicativo.
**blue/service.yaml:** Descrição do Service da versão azul do seu aplicativo.
**green/deployment.yaml:** Descrição do Deploy/ment da versão verde do seu aplicativo.
**green/service.yaml:** Descrição do Service da versão verde do seu aplicativo.
**overlays/bluegreen/ingress.yaml:** Descrição do Ingress para rotear o tráfego entre as versões azul e verde.

## Certifique-se de ajustar os nomes e conteúdos dos arquivos conforme necessário para o seu aplicativo.

### Configure o arquivo ***base/kustomization.yaml*** para incluir os recursos base do seu aplicativo. Por exemplo:

```
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - deployment.yaml
  - service.yaml

```  

Configure o arquivo ***blue/kustomization.yaml*** para incluir os recursos da versão azul do seu aplicativo. Por exemplo:

```
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - ../base
  - deployment.yaml
  - service.yaml

```

### Configure o arquivo green/kustomization.yaml para incluir os recursos da versão verde do seu aplicativo. Por exemplo:

```
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - ../base
  - deployment.yaml
  - service.yaml
```

### Crie o arquivo ***overlays/bluegreen/kustomization.yaml*** para configurar o overlay Blue-Green. Por exemplo:

```
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - ../../blue
  - ../../green
  - ingress.yaml
```  
### Execute o seguinte comando para implantar a versão azul do seu aplicativo no Argo CD:


#### kubectl apply -n argocd -f blue/

## Execute o seguinte comando para implantar a versão verde do seu aplicativo no Argo CD:

#### kubectl apply -n argocd -f green/

## Acesse o painel do Argo CD para criar e configurar a aplicação Blue-Green. Você pode usar a interface web ou o CLI do Argo CD para fazer isso.

###Na configuração da aplicação Blue-Green, configure o campo spec.source.targetRevision para alternar entre a versão azul e verde do seu aplicativo. Por exemplo:

```
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: meu-app-bluegreen
  namespace: argocd
spec:
  source:
    repoURL: <
```    
